FROM docker.io/fedora:41
# get latest swag
RUN dnf update --assumeyes
RUN dnf --assumeyes upgrade --exclude=zchunk-libs --exclude=setup
RUN dnf --assumeyes install ant bc file git java-1.8.0-openjdk jq lftp make npm openssl openssh-clients patch pip unzip wget which xmlstarlet zip
# add yq - a yaml processor
RUN curl -L https://github.com/mikefarah/yq/releases/download/v4.35.1/yq_linux_amd64 -o /usr/bin/yq && chmod +x /usr/bin/yq
# install helm v3.8.1 as of today ;-)
RUN curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash
# install some nice python libs
RUN pip install boto3 python-dotenv requests sentry-sdk tqdm xmltodict lxml GitPython python-magic
# and Saxon, of course
RUN curl -L "https://github.com/Saxonica/Saxon-HE/releases/download/SaxonHE12-4/SaxonHE12-4J.zip" -o /tmp/SaxonHE12.zip && unzip /tmp/SaxonHE12.zip -d /opt/saxon
