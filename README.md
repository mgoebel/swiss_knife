# Swiss Knife

multi-purpose docker images

## build
```
docker build --file fedora.Dockerfile --tag harbor.gwdg.de/sub-fe/mgoebel/fedora . --network=host
```

## push
```
docker push docker.gitlab.gwdg.de/mgoebel/swiss_knife/fedora
```